import "./App.css";
import { useState, useEffect } from "react";

function App() {
  const [user, setUser] = useState("");
  const [pass, setPass] = useState("");

  function handleMessage(event) {
    const parentUrl = "http://localhost:3000";

    if (event.origin === parentUrl) {
      localStorage.setItem("cred", JSON.stringify(event.data));
      window.parent.postMessage("/home", "*");

      console.log("Data dari parent A : ", event.data);
    }
  }

  useEffect(() => {
    window.addEventListener("message", handleMessage);
    const getData = JSON.parse(localStorage.getItem("cred"));
    if (localStorage.getItem("cred") && getData) {
      setUser(getData.user);
      setPass(getData.pass);
    }
  }, []);

  const handleClickLogout = () => {
    // Send to parent A
    window.parent.postMessage("/", "*");
    localStorage.clear();
  };

  return (
    <div className="App">
      {localStorage.getItem("cred") &&
        JSON.parse(localStorage.getItem("cred"))?.user && (
          <header className="App-header">
            <h5>User : {user}</h5>
            <h5>Password : {pass}</h5>
            <br />
            <div>
              <button onClick={handleClickLogout}>Logout</button>
            </div>
          </header>
        )}
    </div>
  );
}

export default App;
