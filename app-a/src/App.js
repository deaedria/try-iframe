import React, { useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import "./App.css";

function App() {
  const navigate = useNavigate();
  const iframeRef = useRef(null);
  const userRef = useRef(null);
  const passRef = useRef(null);

  function handleMessage(event) {
    const childUrl = "http://localhost:4000";
    if (event.origin === childUrl) {
      navigate("/home");
    }
  }

  useEffect(() => {
    const getData = JSON.parse(localStorage.getItem("cred"));
    if (localStorage.getItem("cred") && getData?.user) {
      navigate("/home");
    }
    window.addEventListener("message", handleMessage);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClickSend = () => {
    // Send to child B
    const inputValue = {
      user: userRef.current.value,
      pass: passRef.current.value,
    };
    if (inputValue.user && inputValue.pass) {
      localStorage.setItem("cred", JSON.stringify(inputValue));
      iframeRef.current?.contentWindow?.postMessage(inputValue, "*");
    }
  };

  return (
    <div className="App">
      <div style={{ border: "10px solid blue" }}>
        <header className="App-header">
          <h3>Login</h3>
          <div>
            <input ref={userRef} placeholder="username" />
            <br /> <br />
            <input ref={passRef} placeholder="password" />
            <br /> <br />
            <button onClick={handleClickSend}>Login</button>
          </div>

          <br />
          <iframe
            ref={iframeRef}
            src="http://localhost:4000"
            style={{ width: "100%", height: "0" }}
            title="Iframe"
            frameBorder="0"
          ></iframe>
        </header>
      </div>
    </div>
  );
}

export default App;
