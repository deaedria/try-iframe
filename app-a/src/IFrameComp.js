import React, { forwardRef, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";

const IFrameComp = forwardRef((props, ref) => {
  const navigate = useNavigate();
  const iframeRef = useRef(null);

  function handleMessage(event) {
    const childUrl = "http://localhost:4000";
    if (event.origin === childUrl) {
      if (event.data === "/") {
        localStorage.clear();
        navigate("/");
      }
    }
  }

  useEffect(() => {
    if (!localStorage.getItem("cred")) navigate("/");
    window.addEventListener("message", handleMessage);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <iframe
      ref={iframeRef}
      src="http://localhost:4000"
      style={{
        width: "100%",
        height: "100vh",
      }}
      title="Iframe"
      frameBorder="0"
    ></iframe>
  );
});

export default IFrameComp;
